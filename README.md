# Trabalho Final de Programacao #
Computacao - UFC

# Componentes #
JEFFERSON BRUNO TORRES DE MENEZES - 374185
MARCOS CARNEIRO LIMA              - 374194
MACOS JOSE MARTINS PEREIRA FILHO  - 374195

# Instrucoes
Executar o comando 'make' dentro da pasta src,
para executar o programa, ./batalha_naval

A evolucao do projeto pode ser acompanhada cronologicamente
com o comando 'git log --reverse'


# Link para o repositorio
https://bitbucket.org/jjbruno/trabalho_final_programacao
