
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "tabuleiro.h"
#include "embarcacoes.h"
#include "interface.h"
#include "ia.h"


// Inicializa a Memoria das jogadas passadas
void inicializar_memoria(Memoria* memoria)
{
    memoria->inicial = NULL;
    memoria->ultimo = NULL;
    memoria->sentido = 0;
}

// Procura por uma Embarcacao que tenha sido atacada mas nao destruida
Memoria* procurar_pendente(Tabuleiro* tabuleiro)
{
    No* it = tabuleiro->primeiro;
    while(it)
    {
        if(it->atacado && it->embarcacao && it->embarcacao->pontos_vida > 0)
        {
            Memoria* memoria = malloc(sizeof(Memoria));
            memoria->inicial = it;
            memoria->ultimo  = it;
            memoria->sentido = 0;
            return memoria;
        }
        it = proximo(it);
    }
    return NULL;
}

// Ataca o tabuleiro do humano enquanto acertar uma embarcacao
void jogada_computador(Tabuleiro* humano, Tabuleiro* comp, Memoria* memoria)
{
    int l = 0, c = 0, retorno = -1;
    No* alvo = NULL;

    // Procurar por embarcacoes ainda nao destruidas
    Memoria* esquecida = procurar_pendente(humano);
    if(memoria->inicial == NULL && esquecida)
    {
        memoria->inicial = esquecida->inicial;
        memoria->ultimo  = esquecida->ultimo;
        memoria->sentido = 0;
    }

    // Primeira jogada, procurar aleatorio
    if(memoria->inicial == NULL)
    {
        do
        {
            l = rand()%10 + 1;
            c = rand()%10 + 1;
            alvo = buscar(humano, l, c);

        } while(alvo == NULL || alvo->atacado == 1);
        humano->ultima_linha_atacada = l;
        humano->ultima_coluna_atacada = c;
        retorno = tiro(humano, alvo);

        if(retorno == ACERTOU)
        {
            memoria->inicial = alvo;
            memoria->ultimo  = alvo;
        }

        if(retorno == ACERTOU_JANGADA)
        {
            int sacrificou = sacrificar_submarino(comp);
        }
    }
    // Achamos o primeiro ponto da Embarcacao
    else
    {
        int sentido = memoria->sentido;
        alvo = memoria->ultimo;

        /////
            if((sentido == 0 || sentido == BAIXO) && alvo->inferior && !alvo->inferior->atacado)
            {
                alvo = alvo->inferior;
                sentido = BAIXO;
                retorno = tiro(humano, alvo);
            }
            else
            {
                memoria->sentido = 0;
                memoria->ultimo = memoria->inicial;
            }

            if((sentido == 0 || sentido == CIMA) && alvo->superior && !alvo->superior->atacado)
            {
                alvo = alvo->superior;
                sentido = CIMA;
                retorno = tiro(humano, alvo);
            }
            else
            {
                memoria->sentido = 0;
                memoria->ultimo = memoria->inicial;
            }

            if((sentido == 0 || sentido == DIREITA) && alvo->direito && !alvo->direito->atacado)
            {
                alvo = alvo->direito;
                sentido = DIREITA;
                retorno = tiro(humano, alvo);
            }
            else
            {
                memoria->sentido = 0;
                memoria->ultimo = memoria->inicial;
            }

            if((sentido == 0 || sentido == ESQUERDA) && alvo->esquerdo && !alvo->esquerdo->atacado)
            {
                alvo = alvo->esquerdo;
                sentido = ESQUERDA;
                retorno = tiro(humano, alvo);
            }
            else
            {
                memoria->sentido = 0;
                memoria->ultimo = memoria->inicial;
            }
            /////

        if(retorno == DESTRUIU || retorno == ACERTOU_JANGADA)
        {
            if(retorno == ACERTOU_JANGADA)
            {
                int sacrificou = sacrificar_submarino(comp);
            }
            memoria->inicial = NULL;
            memoria->ultimo  = NULL;
            memoria->sentido = 0;
        }

        if(retorno == ACERTOU)
        {
            memoria->sentido = sentido;
            memoria->ultimo = alvo;
        }

        if(retorno == ERROU)
        {
            memoria->ultimo  = memoria->inicial;
            memoria->sentido = 0;
        }

        humano->ultima_linha_atacada = alvo->linha;
        humano->ultima_coluna_atacada = alvo->coluna;
    }
    if(retorno == 1 || retorno == -1 || retorno == 2)
        jogada_computador(humano,comp,memoria);
}


