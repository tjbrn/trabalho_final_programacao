#ifndef ENTRADA_DADOS_H
#define ENTRADA_DADOS_H

// Forward declaration
typedef struct Tabuleiro Tabuleiro;

void limpar_tela();

void limpar_buffer();

void pausar_tela();

// Exige do usuario coordenadas validas
int receber_coordenadas(Tabuleiro*,Tabuleiro*);

// Exibe os 2 Tabuleiros
void exibir2(Tabuleiro*, Tabuleiro*);

#endif
