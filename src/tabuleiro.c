
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "embarcacoes.h"
#include "tabuleiro.h"

No* buscar(Tabuleiro* tabuleiro, int linha, int coluna)
{
    if(linha <= 0 || coluna <= 0)
        return NULL;
    if(linha > DP || coluna > DP)
        return NULL;

    No* it = tabuleiro->primeiro;

    // O No it aponta para o primeiro No de uma sequencia de de 100 No's
    // Sabendo disso, e considerando o fato de que o tabuleiro tem dimensao
    // 10 por 10, podemos trata-lo como uma matriz usando a notacao (i*DIMENSAO+j)
    return it + (DP * (linha - 1) + (coluna - 1));
}

No* proximo(No* no)
{
    if(no == NULL)
        return NULL;

    if(no->direito == NULL && no->inferior == NULL)
        return NULL;

    // Vizualizando o Tabuleiro como uma sequencia de 100 ponteiros, podemos
    // usar a aritmetica de ponteiros para retornar o No imediatamente a direita
    // apenas incrementando 1 ao ponteiro atual. Caso ele seja o ultimo da sua
    // linha, ele ira retornar o primeiro da linha inferior, pois essa foi a
    // ordem de distribuicao dos No's usada em inicializar(Tabuleiro**).
    // No caso de chamar proximo(ultimo_no) , o retorno sera Nulo.
    return no+1;
}

int vivo(Tabuleiro* tabuleiro)
{
    // O Tabuleiro possui um contador indicando quantas Embarcacoes jogaveis
    // foram distribuidas nele(JANGADA nao conta como Embarcacao jogavel)
    // Quando uma Embarcacao eh destruida, a variavel
    // Tabuleiro->embarcacoes_destruidas eh incrementada,
    // Assim, enquanto "embarcacoes" for mais que "embarcacoes_destruidas",
    // podemos afirmar que ainda existem Embarcacoes jogaveis no tabuleiro
    return tabuleiro->embarcacoes > tabuleiro->embarcacoes_destruidas;
}

void inicializar(Tabuleiro** tabuleiro, int jogador)
{
    // O Tabuleiro tem dimensoes DP X DP (No caso, 10 x 10)
    // Sao alocados 100 No's para o Tabuleiro
    No* no = malloc(DP*DP*sizeof(No));

    // Iremos percorrer por todos os No's. "i" sera sua linha, e "j" sera a
    // sua coluna. Sabendo disso, podemos usar a logica DIMENSAO*i+j para achar
    // a sua posicao exata na memoria. Cada No tera suas variaveis inicializadas
    // de forma que no final do laco teremos um tabuleiro vazio perfeitamente
    // valido alocado na memoria.
    int i,j, ATUAL;
    for (i = 0; i < DP; ++i)
    {
        for (j = 0; j < DP; ++j)
        {
            ATUAL = DP*i+j;

            no[ATUAL].icone      = ' ';
            no[ATUAL].atacado   = 0;
            no[ATUAL].embarcacao = NULL;

            no[ATUAL].linha = i+1;
            no[ATUAL].coluna = j+1;

            if(i == 0)
                no[ATUAL].superior = NULL;
            else
                no[ATUAL].superior = &no[ATUAL-DP];

            if(i == DP-1)
                no[ATUAL].inferior = NULL;
            else
                no[ATUAL].inferior = &no[ATUAL+DP];
            if(j == 0)
                no[ATUAL].esquerdo = NULL;
            else
                no[ATUAL].esquerdo = &no[ATUAL-1];

            if(j == DP-1)
                no[ATUAL].direito = NULL;
            else
                no[ATUAL].direito = &no[ATUAL+1];
        }
    }
    // Ainda eh necessario mudar a direcao do ponteiro tabuleiro recebido
    // como argumento, para isso contruiremos um Tabuleiro init que tem como
    // primeiro No o Tabuleiro de 100 No's criado anteriormente, e suas variaveis
    // necessarias para inicializacao, como a qual jogador pertence o tabuleiro,
    // e o numero de embarcacoes, que vai ser zero.
    Tabuleiro init = {&no[0], jogador, 0, 0, 0, 0, NULL};
    *tabuleiro     = malloc(sizeof(Tabuleiro));
    memcpy(*tabuleiro, &init, sizeof(Tabuleiro));
}

void resetar(Tabuleiro* tabuleiro)
{
    // Simplesmente vai resetar o tabuleiro, de formar que ao fim do procedimento
    // o tabuleiro sera um Tabuleiro vazio perfeitamente valido

    tabuleiro->embarcacoes = 0;
    tabuleiro->embarcacoes_destruidas = 0;

    No* it = tabuleiro->primeiro;
    while(it)
    {
        it->atacado  = 0;
        it->icone      = ' ';
        it->embarcacao = NULL;
        it = proximo(it);
    }
}

int tiro(Tabuleiro* tabuleiro, No* alvo)
{
    // Funcao tiro() vai receber um alvo valido que nao tenha sido atacado
    // previamente, vai atacar esse alvo, tendo 4 possibilidades :
    //                              errar, acertar, destruir, acertar jangada.
    // Eh na funcao tiro que ocorre a manipulacao dos icones de cada No.
    // Por exemplo, colocando 'O' caso tenha acertado o Mar.
    // No caso de destruir uma Embarcacao, a funcao destruir() sera chamada.
    // Ao fim, sera retornado ERROU,ACERTOU,DESTRUIU,ACERTOU_JANGADA de acordo
    // com as possibilidades ocorridas.

    alvo->atacado = 1;

    if(alvo->embarcacao == NULL)
    {
        alvo->icone = 'O';
        return ERROU;
    }

    alvo->embarcacao->pontos_vida -= 1;

    alvo->icone = '*';

    if(alvo->embarcacao->pontos_vida == 0)
    {
        int tipo = alvo->embarcacao->tipo;
        destruir(alvo->embarcacao, tabuleiro);
        if(tipo == JANGADA)
            return ACERTOU_JANGADA;
        return DESTRUIU;
    }

    return ACERTOU;
}

void destruir(Embarcacao* embarcacao, Tabuleiro* tabuleiro)
{
    // Eh a funcao responsavel por eliminar uma Embarcacao do Tabuleiro,
    // assim como libera-la da memoria.

    // O Tabuleiro tem uma variavel que pode armazenar uma mensagem para ser
    // exibida posteriormente. Essa variavel sera utilizada para armazenar qual
    // Embarcacao foi destruida.
    if(tabuleiro->jogador == 1)
    {
        if(embarcacao->tipo == PORTA_AVIOES)
            tabuleiro->mensagem = "O computador acabou de destruir um porta avioes";
        if(embarcacao->tipo == DESTROYER)
            tabuleiro->mensagem = "O computador acabou de destruir um destroyer";
        if(embarcacao->tipo == FRAGATA)
            tabuleiro->mensagem = "O computador acabou de destruir uma fragata";
        if(embarcacao->tipo == SUBMARINO)
            tabuleiro->mensagem = "O computador acabou de destruir um submarino";
        if(embarcacao->tipo == JANGADA)
            tabuleiro->mensagem = "O computador acabou de destruir uma jangada";
    }
    if(tabuleiro->jogador == 2)
    {
        if(embarcacao->tipo == PORTA_AVIOES)
            tabuleiro->mensagem = "O humano acabou de destruir um porta avioes";
        if(embarcacao->tipo == DESTROYER)
            tabuleiro->mensagem = "O humano acabou de destruir um destroyer";
        if(embarcacao->tipo == FRAGATA)
            tabuleiro->mensagem = "O humano acabou de destruir uma fragata";
        if(embarcacao->tipo == SUBMARINO)
            tabuleiro->mensagem = "O humano acabou de destruir um submarino";
        if(embarcacao->tipo == JANGADA)
            tabuleiro->mensagem = "O humano acabou de destruir uma jangada";
    }

    // JANGADA nao conta como Embarcacao jogavel
    if(embarcacao->tipo != JANGADA)
        tabuleiro->embarcacoes_destruidas += 1;

    // Como por padrao o Tabuleiro do jogador 2(computador) esta oculto,
    // ao destruir uma embarcacao revelaremos qual Embarcacao estave presente.
    if(tabuleiro->jogador == 2)
        inserir_imagem(embarcacao, tabuleiro);

    No* no = embarcacao->head;
    int i;
    // Percorreremos todos os No's que apontam para essa mesma Embarcacao,
    // e faremos aponta-lo para Nulo, pois a Embarcacao sera liberada da memoria.
    if(embarcacao->sentido == VERTICAL)
    {
        for (i = 0; i < embarcacao->tamanho; ++i)
        {
            no->embarcacao = NULL;
            no = no->inferior;
        }
    }
    if(embarcacao->sentido == HORIZONTAL)
    {
        for (i = 0; i < embarcacao->tamanho; ++i)
        {
            no->embarcacao = NULL;
            no = no->direito;
        }
    }
    free(embarcacao);
}

void inserir_imagem(Embarcacao* embarcacao, Tabuleiro* tabuleiro)
{
    // Vai simplesmente inserir a imagem da Embarcacao nos No's do Tabuleiro

    No* no = embarcacao->head;
    int i;
    if(embarcacao->sentido == VERTICAL)
    {
        for (i = 0; i < embarcacao->tamanho; ++i)
        {
            no->icone = embarcacao->imagem[i];
            no = no->inferior;
        }
    }
    if(embarcacao->sentido == HORIZONTAL)
    {
        for (i = 0; i < embarcacao->tamanho; ++i)
        {
            no->icone = embarcacao->imagem[i];
            no = no->direito;
        }
    }
}

int sacrificar_submarino(Tabuleiro* tabuleiro)
{
    // Vai sacrificar um submarino, caso ainda exista, do Tabuleiro.
    No* it = tabuleiro->primeiro;

    // Procuraremos um submarino por todo  o Tabuleiro
    while(it)
    {
        // TODO arrumar uns detalhes aqui
        // "it" existe, possui uma embarcacao, e essa embarcacao eh um submarino
        // Achamos nosso submarino que vai ser sacrificado.
        if(it && it->embarcacao && it->embarcacao->tipo == SUBMARINO && it->embarcacao->pontos_vida > 0)
        {
            if(tabuleiro->jogador == 1)
                it->icone = '*';
            if(tabuleiro->jogador == 2)
                it->icone = '@';

            Embarcacao* submarino = it->embarcacao;
            submarino->pontos_vida = 0;
            free(submarino);

            it->atacado = 1;
            it->embarcacao = NULL;

            tabuleiro->embarcacoes_destruidas += 1;
            return 1;
        }
        it = proximo(it);
    }
    // it == NULL...
    return 0;
}
