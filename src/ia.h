#ifndef IA_H
#define IA_H
#endif

#define DIREITA 1
#define ESQUERDA 2
#define CIMA 3
#define BAIXO 4

// Forward declaration
typedef struct Memoria Memoria;

struct Memoria
{
    No* inicial;
    No* ultimo;
    int sentido;
};

// Inicializa o tabuleiro
void inicializar_memoria(Memoria*);

// Inicializa a memoria das jogadas
Memoria* procurar_pendente(Tabuleiro*);

// Jogada do IA
void jogada_computador(Tabuleiro *, Tabuleiro*, Memoria*);
