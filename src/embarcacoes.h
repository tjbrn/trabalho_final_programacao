#include "tabuleiro.h"

#ifndef EMBARCACOES_H
#define EMBARCACOES_H

#define VAZIO        0
#define PORTA_AVIOES 1
#define DESTROYER    2
#define FRAGATA      3
#define SUBMARINO    4
#define JANGADA      5
#define HORIZONTAL   8
#define VERTICAL     9

// Forward declaration
typedef struct No No;

typedef struct Embarcacao
{
    int tipo;
    int pontos_vida;
    int tamanho;
    int sentido;
    char* imagem;
    No* head;
} Embarcacao;


void distribuirEmbarcacoes(Tabuleiro**);

void criarEmbarcacao(Tabuleiro **, int);

// Inicializa as embarcacoes
void inicializar_embarcacao(Embarcacao**,No*,int, int);


#endif
