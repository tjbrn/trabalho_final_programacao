#ifndef TABULEIRO_H
#define TABULEIRO_H

#define DP 10 // dimensao padrao do tabuleiro
#define ACERTOU 1
#define ERROU 0
#define DESTRUIU 2
#define ACERTOU_JANGADA 3

// Forward declaration
typedef struct Embarcacao Embarcacao;
typedef struct Tabuleiro Tabuleiro;
typedef struct No No;

struct No
{
    char icone;
    int atacado;
    No* superior;
    No* inferior;
    No* esquerdo;
    No* direito;
    Embarcacao* embarcacao;
    int linha;
    int coluna;
};

struct Tabuleiro
{
    No* primeiro;    // aponta para o primeir No do Tabuleiro
    int jogador;     // indica a qual jogador pertence o Tabuleiro
    int embarcacoes; // quantidade total de embarcacoes
    int embarcacoes_destruidas; // embarcacoes que foram destruidas
    int ultima_linha_atacada;
    int ultima_coluna_atacada;
    char* mensagem;
};

// Busca e retorna um No (coordenadas entre 1 e DIMENSAO)
No* buscar(Tabuleiro*, int, int);

// Retorna o proximo No(direito ou primeiro da linha inferior)
No* proximo(No*);

// Retorna se ainda existem Embarcacoes jogaveis vivas no tabuleiro
int vivo(Tabuleiro*);

// Inicializa o Tabuleiro e suas variaveis necessarias
void inicializar(Tabuleiro**,int);

// Reseta o Tabuleiro apos o fim de uma rodada
void resetar(Tabuleiro*);

// Ataca uma No
int tiro(Tabuleiro*, No*);

// Destroi uma Embarcacao presente no Tabuleiro
void destruir(Embarcacao*, Tabuleiro*);

// Insere a imagem de uma embarcacao no Tabuleiro
void inserir_imagem(Embarcacao* embarcacao, Tabuleiro* tabuleiro);

// Procura por um submarino no Tabuleiro e o destroi
int sacrificar_submarino(Tabuleiro*);

#endif
