#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "tabuleiro.h"
#include "interface.h"

void limpar_tela()
{
#ifndef _WIN32
    system("clear");
#else
    system("cls");
#endif
}

void limpar_buffer()
{
    int c;
    while ( (c = getchar()) != '\n' && c != EOF );
}

void pausar_tela()
{
    printf("\nPressione ENTER para continuar ...\n");
    char c = getchar();
    if(c != '\n') limpar_buffer();
}


//Essa função eh responsavel por toda a jogada humana, desde
// a entrada das coordenadas ateh ele errar ou acertar uma jangada.
int receber_coordenadas(Tabuleiro *humano, Tabuleiro *comp)
{
    char* jogada = malloc(sizeof(char*)*12);
    char c;
    int i = 0, linha = 0, coluna = 0, tamanho = 0;
    int retorno = 0;
    No *no;

    do{//Dentro desse do while vai se passar todo o processo da
    // jogada humana, ateh ele perder a sua vez

        //Caso o jogador tenha acertado,
        // o tabuleiro sera exibido com a atualizacao, se ele errar 
        // o tabuleiro sera exibido no main.
        if(retorno == 1 || retorno == 2)
        {
            exibir2(humano, comp);
            printf("\nVoce acertou uma embarcacao, jogue novamente!\n");
        }

        do{ 

            if(linha < 0 || coluna < 0){//Caso o jogador insira algo invalido,
            // a coluna ou a linha vao ser igual a -1, o que vai fazer com
            // que essa mensagem seja exibida.
                printf("\nJogada invalida!\n");
                linha = 0;
                coluna = 0;
            }
                

            printf("\nDigite a sua jogada: ");
            fgets(jogada, 10, stdin);

            tamanho = strlen(jogada);
            //Esse if garante que o buffer seja limpo, caso o jogador
            // insira uma string muito longa como jogada.
            if(tamanho >= 9 && jogada[tamanho-1] != '\n'){
                limpar_buffer();
            }
                

            jogada[tamanho - 1] = '\0';

            tamanho = strlen(jogada);
            //Auxiliam na deteccao de uma jogada invalida.
            linha = -1;
            coluna = -1;

            //O laco continua/inicia se a string da jogada for maior
            // que 1 e se o contador for menor que o tamanho da string.
            while(tamanho > 1 && i < tamanho){//Nesse while sera
            // verificado a validez da jogada.
            //No tabuleiro as linhas e colunas sao determinadas por numeros
            // inteiros, entao eh feita a adaptacao da jogada do usuario.
                c = toupper(jogada[i]);

                if(c == 'A')
                    coluna = 1;
                else if(c == 'B')
                    coluna = 2;
                else if(c == 'C')
                    coluna = 3;
                else if(c == 'D')
                    coluna = 4;
                else if(c == 'E')
                    coluna = 5;
                else if(c == 'F')
                    coluna = 6;
                else if(c == 'G')
                    coluna = 7;
                else if(c == 'H')
                    coluna = 8;
                else if(c == 'I')
                    coluna = 9;
                else if(c == 'J')
                    coluna = 10;

                else if(c == '1'){//Caso seja encontrado 1 no inicio da
                // string eh verificado se o proximo caractere eh 0,
                    linha = 1;    //caso seja 0, entao eh um 10.
                    if(jogada[i+1] != '\0' && jogada[i+1] == '0')
                        linha = 10;
                }   
                else if(c == '2')
                    linha = 2;
                else if(c == '3')
                    linha = 3;
                else if(c == '4')
                    linha = 4;
                else if(c == '5')
                    linha = 5;
                else if(c == '6')
                    linha = 6;
                else if(c == '7')
                    linha = 7;
                else if(c == '8')
                    linha = 8;
                else if(c == '9')
                    linha = 9;
                else if(i == tamanho-1 && c == '0' && jogada[i-1] == '1')
                    linha = 10;//Se estiver no final da string, for encontrado
                    // um 0 e o caractere anterior for 1, entao eh um 10.
                
                i+= tamanho-1;//Isso faz com que o contador vah para o fim da string.
            }

            i = 0;
        //Como linha e coluna sao iguais a -1 no antes de entrar no laco,
        // isso significa que se eles tiverem sido alterados,
        // a jogada eh valida, entao sai do laco.
        }while(linha <= 0 || coluna <= 0);

        no = buscar(comp, linha, coluna);//Eh feito uma busca pelo no
        // que possui as coordenadas informadas pelo jogador.

        if(!no->atacado){//Eh verificado se o no ainda nao foi atacado.

            comp->ultima_linha_atacada = linha;
            comp->ultima_coluna_atacada = coluna;
            //A funcao tiro recebe o tabuleiro o qual deseja-se atingir
            // e em qual no se quer atingir.
            //Ela retorna 0 - se errar, 1 - se acertar, 2 - se acertar
            // e destruir uma embarcacao e 3 - se acertar a jangada.
            retorno = tiro(comp, no);
        }
        else{//Caso o no jah tenha sido atacado, eh exibida
        // uma mensagem e retorno recebe 4.
            printf("\nEsse alvo ja foi atacado. Insira novas coordenadas!\n");
            retorno = 4;//retorno = 4 faz com que todo o processo seja recomecado.
        }

        linha = 0;
        coluna = 0;

    }while(retorno == 1 || retorno == 2 || retorno == 4);//Se o jogador tiver
    // acertado alguma embarcacao ou atirado em lugar que jah
    // foi atingindo, o processo eh recomecado.
    
    if(retorno == 3){//se o jogador tiver acertado uma jangada,
    // ele perde um de seus submarinos, caso tenha algum submarino.
        sacrificar_submarino(humano);
    }

}

void exibir2(Tabuleiro* t1, Tabuleiro* t2)
{
    limpar_tela();
    int i, j, ATUAL;
    char c;

    // Embarcacoes restantes
    for (i = 0; i < DP/2 - 11; ++i)
        printf(" ");
    printf("Embarcacoes restantes: Humano %i", t1->embarcacoes - t1->embarcacoes_destruidas);
    for (i = 0; i < DP/2; ++i)
        printf(" ");
    printf("Computador %i\n\n\n", t2->embarcacoes - t2->embarcacoes_destruidas);


    // Linha "Humano ... Computador"
    for (i = 0; i < DP/2; ++i)
        printf(" ");
    printf("Humano");
    for (i = 0; i < DP/2; ++i)
        printf(" ");
    printf("Computador\n");

    // Linha "ABCDEFGH" Humano
    printf("   ");
    for (i = 0; i < DP; ++i)
        printf("%c", 'A'+i);
    printf("     ");
    // Linha "ABCDEFGH" Computador
    for (i = 0; i < DP; ++i)
        printf("%c", 'A'+i);
    printf("\n");

    // Linha "+-- ... --+" Humano
    printf("  +");
    for (i = 0; i < DP; ++i)
        printf("-");
    printf("+");
    // Linha "+-- ... --+" Computador
    printf("   +");
    for (i = 0; i < DP; ++i)
        printf("-");
    printf("+");
    printf("\n");


    No* no1 = t1->primeiro;
    No* no2 = t2->primeiro;
    // icones
    for (i = 0; i < DP; ++i)
    {
        printf("%2i|", i+1);
        for (j = 0; j < DP; ++j)
        {
            ATUAL = DP*i+j;

            // No do humano sempre estara revelado, exibir
            // sem checagem
            printf("%c", no1[ATUAL].icone);
        }
        printf("| ");

        printf("%2i|", i+1);
        for (j = 0; j < DP; ++j)
        {
            ATUAL = DP*i+j;

            // Exibir o No do computador apenas se ele tiver
            // sido atacado
            if(no2[ATUAL].atacado)
                printf("%c", no2[ATUAL].icone);
            else
                printf(" ");
        }
        printf("|\n");
    }

    // Linha "+-- ... --+" Humano
    printf("  +");
    for (i = 0; i < DP; ++i)
        printf("-");
    printf("+");
    // Linha "+-- ... --+" Computador
    printf("   +");
    for (i = 0; i < DP; ++i)
        printf("-");
    printf("+\n\n");

    // Mensagens sobre embarcacoes destruidas
    if(t1->mensagem)
    {
        printf("%s\n", t1->mensagem);
        t1->mensagem = NULL;
    }
    if(t2->mensagem)
    {
        printf("%s\n", t2->mensagem);
        t2->mensagem = NULL;
    }
    printf("\n");

    // Linha Ultima jogada
    if(t1->ultima_coluna_atacada != 0 && t1->ultima_linha_atacada != 0)
    {
        char c = 64 + t1->ultima_coluna_atacada;
        int l  = t1->ultima_linha_atacada;
        printf("Ultima jogada do Computador: %c%i\n", c, l);
    }
    if(t2->ultima_coluna_atacada != 0 && t2->ultima_linha_atacada != 0)
    {
        char c = 64 + t2->ultima_coluna_atacada;
        int l  = t2->ultima_linha_atacada;
        printf("Ultima jogada do Humano: %c%i\n", c, l);
    }
}
