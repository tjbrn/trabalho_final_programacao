
#include <stdio.h>
#include <stdlib.h>
#include "embarcacoes.h"
#include "tabuleiro.h"

//Funcao responsavel por construir todas as embarcacoes do jogo
void distribuirEmbarcacoes(Tabuleiro **tabuleiro){

	criarEmbarcacao(tabuleiro, PORTA_AVIOES);
    criarEmbarcacao(tabuleiro, DESTROYER);
    criarEmbarcacao(tabuleiro, DESTROYER);
    criarEmbarcacao(tabuleiro, FRAGATA);
    criarEmbarcacao(tabuleiro, FRAGATA);
    criarEmbarcacao(tabuleiro, FRAGATA);
    criarEmbarcacao(tabuleiro, SUBMARINO);
    criarEmbarcacao(tabuleiro, SUBMARINO);
    criarEmbarcacao(tabuleiro, JANGADA);
}

//Funcao que constroi cada uma das embarcacoes.
//Ela recebe como parametro um Tabuleiro, que será do jogador ou do computador,
//e o tipo de embarcacao.
void criarEmbarcacao(Tabuleiro **tabuleiro, int tipoEmbarcacao){

    // 0 - sentido vertical
    // 1 - sentido horizontal
    int sentido = 0;
    int linha = 0;
    int coluna = 0;
    int cont = -1;
    No *no, *head;

    int tamanho = 0;


    //Atraves do tipo de embarcacao eh determinado o valor da variavel tamanho
    //a qual ira auxiliar na colocacao das embarcacoes de forma aleatoria.
    if(tipoEmbarcacao == PORTA_AVIOES)
        tamanho = 5;
    else if(tipoEmbarcacao == DESTROYER)
        tamanho = 3;
    else if(tipoEmbarcacao == FRAGATA)
        tamanho = 2;
    else if(tipoEmbarcacao == SUBMARINO)
        tamanho = 1;
    else if(tipoEmbarcacao == JANGADA)
        tamanho = 1;

    //Dentro dessa estrutura de repeticao eh determinado o local de
    // insercao da embarcacao.
    while(cont < 0){ //Enquanto o cont for negativo significa q ainda
    // nao foi encontrado um local pra inserir a embarcacao.
        do{
            sentido = rand()%2;// Escolhe de forma aletoria o sentido
            // da embarcacao (1 - horizontal ou 0 - vertical).

            //Definido o sentido, eh decidido de forma aleatoria onde
            // ficara o primeiro no, o no cabeca da embarcacao
            //Porem o no cabeca deve ficar a uma distancia minima - que eh
            // do tamanho da embarcacao a qual pertence -
            // da borda do tabuleiro, para assim poder caber toda a embarcacao.
            if(sentido){//Se o sentido for horizontal então a embarcacao deve ficar
            // a uma distancia da borda direita do tamanho da embarcacao.
                linha = (rand()%10)+1; 
                coluna = (rand()%(10-tamanho))+1;
                sentido = HORIZONTAL;
            }
            else{//Se o sentido for vertical então a embarcacao deve ficar
            // a uma distancia da borda inferior do tamanho da embarcacao.
                linha = (rand()%(10-tamanho))+1;
                coluna = (rand()%10)+1;
                sentido = VERTICAL;
            }

            //A funcao buscar retorna qual  no do tabuleiro possui aquelas coordenadas.
            no = buscar(*tabuleiro, linha, coluna);

        }while(no->icone != ' ');//Isso ira se repetir enquanto nao
        // for encontrado um no vazio, sem embarcacoes.

        head = no;
        cont = 0;
        //Apos encontrar um no vazio, eh necessario garantir que os nos a direita
        // ou abaixo dele podem armazenar todo o restante da embarcacao.
        //Nesse while, iremos garantir que podemos inserir embarcacao.
        while(cont < tamanho && cont >= 0){
            if(no->icone == ' '){//Se o no estiver vazio avanca para o no direito,
            // se o sentido for horizontal, ou para o no inferior, caso contrario.
                if(sentido == HORIZONTAL)
                    no = no->direito;
                else
                    no = no->inferior;
                cont++;
            }
            else//caso seja encontrado algum no que nao esteja vazio, cont recebe -1,
                //o que significa q todo o processo vai ser recomecado.
                cont = -1;
        }
    }

    //Ao sair das estruturas de repeticao acima, garantimos que ha espaco
    // suficiente para inserir a embarcacao.
    Embarcacao *embarcacao;//Entao criamos uma embarcacao.
    //A funcao incializar_embarcacao() recebe com parametro uma embarcacao,
    // o primeiro no dela, o tipo e o sentido, e inicializa a embarcacao.
    inicializar_embarcacao(&embarcacao, head, tipoEmbarcacao, sentido);
    inserir_imagem(embarcacao, *tabuleiro);//Insere em cada no os seus respectivos icones, simbolos.

    cont = 0;
    no = head;
    //Esse while faz com que os nos no qual foram inseridos a embarcacao apontem para ela.
    while(cont < tamanho){
        no->embarcacao = embarcacao;
        
        if(sentido == HORIZONTAL) 
            no = no->direito;
        else
            no = no->inferior;
            
        cont++;
    }

    if(embarcacao->tipo != JANGADA)
        (*tabuleiro)->embarcacoes += 1;
}


void inicializar_embarcacao(Embarcacao** embarcacao,No* head, int tipo, int sentido)
{
    // Vai inicializar a embarcacao, de acordo com o tip e o sentido recebido,
    // fazendo o ponteiro recebido apontar para a nova embarcacao criada na
    // memoria

    (*embarcacao)          = malloc(sizeof(Embarcacao));
    (*embarcacao)->tipo    = tipo;
    (*embarcacao)->sentido = sentido;
    (*embarcacao)->head    = head;
    if(tipo == PORTA_AVIOES)
    {
        (*embarcacao)->pontos_vida = 5;
        (*embarcacao)->tamanho     = 5;
        if(sentido == VERTICAL)
            (*embarcacao)->imagem = "^###v";
        if(sentido == HORIZONTAL)
            (*embarcacao)->imagem = "<###>";
    }
    if(tipo == DESTROYER)
    {
        (*embarcacao)->pontos_vida = 3;
        (*embarcacao)->tamanho     = 3;
        if(sentido == VERTICAL)
            (*embarcacao)->imagem = "^#v";
        if(sentido == HORIZONTAL)
            (*embarcacao)->imagem = "<#>";
    }
    if(tipo == FRAGATA)
    {
        (*embarcacao)->pontos_vida = 2;
        (*embarcacao)->tamanho     = 2;
        if(sentido == VERTICAL)
            (*embarcacao)->imagem = "^v";
        if(sentido == HORIZONTAL)
            (*embarcacao)->imagem = "<>";
    }
    if(tipo == SUBMARINO)
    {
        (*embarcacao)->pontos_vida = 1;
        (*embarcacao)->tamanho     = 1;
        (*embarcacao)->imagem      = "@";
    }
    if(tipo == JANGADA)
    {
        (*embarcacao)->pontos_vida = 1;
        (*embarcacao)->tamanho     = 1;
        (*embarcacao)->imagem      = "&";
    }
}
