
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include "tabuleiro.h"
#include "embarcacoes.h"
#include "interface.h"
#include "ia.h"

void imagem()
{
    limpar_tela();
    printf("#############################################################################\n"
           "#(@@@@)                    (#########)                   (@@@@@@@@(@@@@@@@@@#\n"
           "#@@@@@@)___                 (####)~~~   /\\                ~~(@@@@@@@(@@@@@@@#\n"
           "#@@@@@@@@@@)                 ~~~~      /::~-__               ~~~(@@@@@@@@)~~#\n"
           "#@@@)~~~~~~  BATALHA NAVAL            /::::::/\\                  ~~(@@@@)   #\n"
           "#~~~                              O  /::::::/::~--,                 ~~~~    #\n"
           "#                                 | /:::::/::::::/{                         #\n"
           "#                 |\\              |/::::/::::::/:::|                        #\n"
           "#                |:/~\\           ||:::/:::::/::::::|                        #\n"
           "#               |,/:::\\          ||/'::: /::::::::::|                       #\n"
           "#              |#__--~~\\        |'#::,,/::::::::: __|   ,,'`,               #\n"
           "#             |__# :::::\\       |-#"":::::::__--~~::| ,'     ',     ,,        #\n"
           "#,    ,,     |____#~~~--\\,'',.  |_#____---~~:::::::::|         ',','  ',    #\n"
           "# '.,'  '.,,'|::::##~~~--\\    `,||#|::::::_____----~~~|         ,,,     '.''#\n"
           "#____________'----###__:::\\_____||#|--~~~~::::: ____--~______,,''___________#\n"
           "#^^^  ^^^^^   |#######\\~~~^^O, | ### __-----~~~~_____########'  ^^^^  ^^^   #\n"
           "#,^^^^^','^^^^,|#########\\_||\\__O###~_######___###########;' ^^^^  ^^^   ^^ #\n"
           "#^^/\\/^^^^/\\/\\^^|#######################################;'/\\/\\/^^^/\\/^^^/\\/^#\n"
           "#   /\\/\\/\\/\\/\\  /\\|####################################'      /\\/\\/\\/\\/\\    #\n"
           "#\\/\\/\\     /\\/\\/\\  /\\/\\/\\/\\    /\\/\\/\\/\\/\\   /\\/\\/\\    /\\/\\/\\/\\      /\\/\\/\\/\\#\n"
           "#\\/\\/\\    /\\/\\/\\/\\    /\\/\\/\\/\\    /\\/\\/\\/\\   /\\/\\/\\/\\    /\\/\\/\\/\\/\\         #\n"
           "#############################################################################\n");
}

int main()
{
    srand(time(NULL));
    limpar_tela();

    // Vai ser usado para armazenar as jogadas do computador
    Memoria memoria;
    inicializar_memoria(&memoria);

    // Tabuleiros
    Tabuleiro* humano;
    Tabuleiro* comp;

    // Inicializando os Tabuleiros
    inicializar(&humano,1);
    inicializar(&comp,2);

    int pontos_j1 = 0;
    int pontos_j2 = 0;
    char op[5], opcao = ' ';

    imagem();
    pausar_tela();

    while(opcao != 'N'){

        resetar(humano);
        resetar(comp);
        distribuirEmbarcacoes(&humano);
        distribuirEmbarcacoes(&comp);

        exibir2(humano, comp);

        while(vivo(humano) && vivo(comp))
        {
            receber_coordenadas(humano, comp);
            jogada_computador(humano, comp, &memoria);
            exibir2(humano, comp);
        }

        if(vivo(humano)){
            pontos_j1 += 1;
            printf("\nVoce venceu\n");
        }
        else{
            pontos_j2 += 1;
            printf("\nComputador venceu\n");
        }

        printf("\n");
        printf("*----Total de vitorias-----*\n");
        printf("* Voce: %d                  *\n", pontos_j1);
        printf("* Computador: %d            *\n", pontos_j2);
        printf("*--------------------------*\n");

        do{


            printf("\nDeseja jogar outra partida?\n");
            printf("[ S ] SIM\n");
            printf("[ N ] NAO\n");

            fgets(op, 3, stdin);
            if(op[0] != '\n' && op[1]!='\n'){limpar_buffer();}

            opcao = toupper(op[0]);
        }while(opcao != 'N' && opcao != 'S');

    }

    imagem();
    printf("Obrigado por jogar !!!\n");

    return 0;
}

